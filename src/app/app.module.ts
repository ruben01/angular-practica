import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';



import { AppComponent } from './app.component';
import { FetchPruebaComponent } from './fetch-prueba/fetch-prueba.component';
import { ImgComponent } from './components/img/img.component';
import { AppRoutingModule } from './app-routing.module';
import { PropertyBindingComponent } from './property-binding/property-binding.component';
import { NgForComponent } from './ng-for/ng-for.component';
import { ProductComponent } from './components/product/product.component';
import { ProductsListComponent } from './components/products-list/products-list.component';
import { HeaderComponent } from './components/header/header.component';
import { HighlightDirective } from './directives/highlight.directive';

@NgModule({
  declarations: [
    AppComponent,
    FetchPruebaComponent,
    ImgComponent,
    PropertyBindingComponent,
    NgForComponent,
    ProductComponent,
    ProductsListComponent,
    HeaderComponent,
    HighlightDirective
  ],
  imports: [
    BrowserModule,
    FormsModule, //modulo para utilizar ngmodel en los input
    HttpClientModule, //modulo para hacer conexiones https a apis
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
