import { Component } from '@angular/core';
import { Product } from './models/products.models'; //importacion del modelo

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})

export class AppComponent {
  
  //Ej: 1
  //dataComponentePadre = 'Soy una propiedad que viene del componente padre';
  dataImgPadre = 'https://source.unsplash.com/random';
    
    showImage = true;
    mostrarImg(){
      this.showImage = !this.showImage;
    }
}
