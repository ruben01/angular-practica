import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router'; //para usar rutas
import { ImgComponent } from './components/img/img.component';
import {FetchPruebaComponent} from './fetch-prueba/fetch-prueba.component'
import { PropertyBindingComponent } from './property-binding/property-binding.component';
import { NgForComponent } from './ng-for/ng-for.component';


const routes: Routes = [
  //{ path: '', redirectTo: '/', pathMatch: 'full' },
  //{ path: '', component: ImgComponent },
  //{ path: 'fetch-prueba', component: FetchPruebaComponent},
  //{ path: 'componente-property-binding', component: PropertyBindingComponent },
  //{ path: 'componente-ng-for', component: NgForComponent }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})

/*@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})*/
export class AppRoutingModule { }
