import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

//este decorador injecta en root
@Injectable({
    providedIn:'root'
})
export class DataService{

    //pasamos la conexion http
    constructor(private http: HttpClient){ }

    //creamos metodo y retornamos la data
    getData(){
        //hacemos uso del fetch
        return fetch('https://rickandmortyapi.com/api/character');
    }
}