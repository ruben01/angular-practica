import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; //1. importamos nuestro modulos para hacer solicitudes a apis
import { Product } from '../models/products.models'; //importamos nuestro modelo para hacer el tipado

@Injectable({
  providedIn: 'root'
})
export class ProductsAPIService {

  //2. injectamos nuestro servicio
  constructor(private http : HttpClient) { }

    //3. crear metodo para traer todos los productos de la api
    getAllProducts(){
      //4. realizamos una peticion get a la url de la api a usar
      //tipamos nuestro salicitud product para no tener propblemas a la hora de almacenar la data en nuestra variable
      return this.http.get<Product[]>('https://fakestoreapi.com/products')
    }
}
