import { Injectable } from '@angular/core';
import { Product } from '../models/products.models';
import { BehaviorSubject } from 'rxjs'; //probando la reactividad rxjs

@Injectable({
  providedIn: 'root'
})
export class TiendaService {
  //lista donde se guardaran nuestros producto seleccionados y sera de tipo "Product"
  //aisalando la logica de nogocios en un service
  //Al implemtar un service, la volvemos privado para que solo sea de uso del servicio
  private miShoppingCart: Product [] = [];

  //creamos nuestro observable de product, esto es para implementar de 
  //manera mas facil a otros componentes donde se necesita esta informacion
  private miCarrito = new BehaviorSubject<Product[]>([]);

  //definimos un observable
  miCarrito$ = this.miCarrito.asObservable();

  //creando metodo para implementar y aislar la logica
  //pasamos como parametro el producto a agregar o eliminar
  agregarProducto(product:Product){
    this.miShoppingCart.push(product)

    //implementamos nuestro observable cuando se cree un product
    this.miCarrito.next(this.miShoppingCart);
  }

  //otra parte de la logica, obtener total del carrito
  obtenerTotal(){
    return this.miShoppingCart.reduce((suma, item) => suma + item.price, 0)
  }

  //accediendo al propiedad de tipo privada a traves de nuestro metodo
  obtenerMiShoppingCart(){
    return this.miShoppingCart;
  }

  constructor() { }
}
