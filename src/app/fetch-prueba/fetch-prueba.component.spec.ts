import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FetchPruebaComponent } from './fetch-prueba.component';

describe('FetchPruebaComponent', () => {
  let component: FetchPruebaComponent;
  let fixture: ComponentFixture<FetchPruebaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FetchPruebaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FetchPruebaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
