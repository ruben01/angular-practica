import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-fetch-prueba',
  templateUrl: './fetch-prueba.component.html',
  styleUrls: ['./fetch-prueba.component.sass']
})
export class FetchPruebaComponent implements OnInit {

  //decimos el tipo de dato any, porque son diferentes tipos en json
  allData:any [] = [];

  //en el constructor resivimos la data url, y pasamos el data service
  constructor(private data: DataService) { }

  ngOnInit(): void {
    //hacemos nuestro obtencion de los datos con promise
    this.data.getData().then((response) => {
        response.json().then((data) => {
            //pasamos la data al la propiedad  para iterar 
            this.allData = data.results
            console.log(this.allData)
        })
    })
    
  }

}
