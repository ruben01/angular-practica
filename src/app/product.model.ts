export interface Product {
    name: string,
    price:number,
    image:string,
    category?:string // ? -> siginifica que al implementar el modelo la propiedad es opcional
}