import { Component, OnInit } from '@angular/core';
import { TiendaService } from 'src/app/service/tienda.service'; //traemos nuestro servicio para usar nuestro observable

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  //variable para utilizar en carrito-nav
  counter = 0

  //para agregar la clase active
  activarMenu = true

  //para agregar la clase active cuando hay click
  Mobile(){
    this.activarMenu = ! this.activarMenu
    console.log(this.activarMenu)
  }

  //injectamos nuestro servicio
  constructor(private tiendaService : TiendaService) { }

  ngOnInit(): void {
    //implementamos el observable para escuchar cambios en el carrito
    this.tiendaService.miCarrito$.subscribe(products => {
      //console.log(products);
      this.counter = products.length
    })
  }

}
