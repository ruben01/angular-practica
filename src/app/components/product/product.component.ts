import { Component,Input,Output,EventEmitter, OnInit } from '@angular/core';
import { Product } from 'src/app/models/products.models';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.sass']
})
export class ProductComponent implements OnInit {

  constructor() {
     
   }

  ngOnInit(): void {
  }

  //@Input envio de informacion de componente padre a hijo
  //creamos las propiedades con valor inicial vacias para poder llenarlas
  @Input('AliasProducto') product: Product = {
    id:0,
    title: '',
    price: 0,
    image: '',
    description: '',
    category: ''
  }
  //con output y eventEmiter emitimos un evento data hacia el componente padre (list-products)
  //en eventEmiter debemos decirle el tipo de informacion que queremos pasar al componente padre
  @Output() agregarProducto = new EventEmitter<Product>();

  //
  agregarCarrito(){
    //ejecutamos el evento emitter
    this.agregarProducto.emit(this.product);
  }
}
