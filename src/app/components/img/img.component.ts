import { Component, Input, OnInit, OnChanges, SimpleChanges, AfterViewInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-img',
  templateUrl: './img.component.html',
  styleUrls: ['./img.component.sass']
})
export class ImgComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {

  //uso del decorador @input
  // a traves del atributo "dataImg" en este ejemplo se comunicaran y pasara la informacion del componete padre al hijo

  //'Esta imagen funciona cuando no hay valor, y la url no existe'
  @Input() dataImg:string = '';

  // si la url no existe o esta mal, pasamos la siguiente con el event binding
  imgDefault = 'https://www.m2crowd.com/core/i/placeholder.png';

  //evento para cargar img default si la url no es buena
  ImgError(){
    this.dataImg = this.imgDefault;
  }


















//--------------------------------------------------------------------------------------------
  //ngDestroy & setInput
  //Ej:
  counter = 0;
  counterFunction: number | undefined;


  //--------------------------------------------------------------------------------------------
  // ciclo de vida de los componentes:

  constructor() {
     //El constructor es lo primero que se ejecuta, antes del render y solo se ejecuta una vez
     //no se deben ejecutar cosas asincronas
     //console.log('constructor', 'imgValue =>', this.dataHijo);
   }
  //----------------------------------------------------------------------------------------------
  
  ngOnChanges(changes: SimpleChanges): void {
    // Este metodo tambien se ejecuta antes del render y durante
    //corre siempre que hay cambios en los input

   // console.log('ngOnChanges', 'imgValue =>', this.dataHijo);
  }

  ngOnInit(): void {
    //corre antes del render, pero que solo correo una vez. Ahi se corren eventos asincronos.

    //console.log('ngOnInit', 'imgValue =>', this.dataHijo);
    /*this.counterFunction = window.setInterval( () => {
      this.counter += 1;
      if(this.counter === 10){
        window.clearInterval(this.counterFunction);
      }
      console.log('run counter');
    }, 1000);*/
    
    
  }

  ngAfterViewInit(): void {
    //ngAfcterViewInit: corre cuando los hijos de ese componentes se han renderizado.
    //console.log('ngAfterViewInit');
  }
  ngOnDestroy() {
    // delete -- once time
    //console.log('ngOnDestroy');
     // window.clearInterval(this.counterFunction);
      
  }


  


}
