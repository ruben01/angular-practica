import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/products.models'; //importacion del modelo
import { TiendaService } from 'src/app/service/tienda.service'; //1. importancion del servicio
import { ProductsAPIService } from 'src/app/service/products-api.service'; // implementado product api

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.sass']
})
export class ProductsListComponent implements OnInit {

  //creando producto para ser utilizado en componete hijo
  //comentamos nuestro items de productos para implementar los de nuestra api
  products: Product [] = [ /*
    {
      id:1,
      name: 'producto 1',
      price: 10,
      image: 'https://planetadelibroscom.cdnstatics2.com/usuaris/libros/fotos/349/m_libros/portada_el-libro-negro-de-las-horas_eva-garcia-saenz-de-urturi_202201141041.jpg'
    },
    {
      id:2,
      name: 'producto 2',
      price: 145,
      image: 'https://planetadelibroscom.cdnstatics2.com/usuaris/libros/fotos/349/m_libros/portada_el-libro-negro-de-las-horas_eva-garcia-saenz-de-urturi_202201141041.jpg'
    },
    {
      id:3,
      name: 'producto 3',
      price: 45,
      image: 'https://planetadelibroscom.cdnstatics2.com/usuaris/libros/fotos/349/m_libros/portada_el-libro-negro-de-las-horas_eva-garcia-saenz-de-urturi_202201141041.jpg'
    }
    , {
      id:4,
      name: 'producto 4',
      price: 100,
      image: 'https://planetadelibroscom.cdnstatics2.com/usuaris/libros/fotos/349/m_libros/portada_el-libro-negro-de-las-horas_eva-garcia-saenz-de-urturi_202201141041.jpg'
    } */
  ]

  //lista donde se guardaran nuestros producto seleccionados y sera de tipo "Product"
  miShoppingCart: Product [] = [];

  //pipes, para ver fecha actual
  today = new Date();
  date = new Date(2021,1,21)

  //calcaular el total de los producto elegidos
  total = 0

  //evento para agregar productos a a shopping cart.
  //este evento recive el producto seleccionado.
  agregarShoppingCart(product: Product){
    //implementando nuestro carrito en el componente
      //this.miShoppingCart.push(product)
      //console.log(this.miShoppingCart)

    //utilizamos el metodo reduce, para sacar el total de precio de los producto
      //this.total = this.miShoppingCart.reduce((suma, item) => suma + item.price, 0)

    //implementando el carrito con nuestro servicio y logica ya separada
    this.tiendaService.agregarProducto(product);
    this.total = this.tiendaService.obtenerTotal();

    
  }

  //2. injectar el servio, es de forma privada
  //injectamos nuestro segundo servicio: productsAPI
  constructor(private tiendaService : TiendaService, private productsApi : ProductsAPIService) { 
    //traemos lo que hay en nuestro shoppingCart
      this.miShoppingCart = this.tiendaService.obtenerMiShoppingCart(); 
  }

  //este metodo es el que nos da angular para manejar cosas asincronas
  ngOnInit(): void {
    //traemos la data de nuestro service
    this.productsApi.getAllProducts()
    .subscribe(data => {
      //data es un array de tipo object, lo tipamos tipo product para que no haya problemas
      this.products = data
      console.log(data);
    })
  }

}
