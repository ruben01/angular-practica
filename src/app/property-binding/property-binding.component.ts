import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-property-binding',
  templateUrl: './property-binding.component.html',
  styleUrls: ['./property-binding.component.sass']
})
export class PropertyBindingComponent implements OnInit {

   //string interpolation, para renderizar -> {{}} -----------------------------
   title = 'soy un string interpolation';
   img = 'https://source.unsplash.com/random';

   //property binding / ej: html -> [disabled] ----------------------------------
   disabled = true;

   //usar event binding / ej: html -> (click) -----------------------------------
   clickDisabled(){
     //cambiamos el estado con -> ! (operador de negacion)
     this.disabled = !this.disabled
   }

   //prueba de *ngIF ------------------------------------------------------------
   show = false
   clickShowMe(){
     this.show = !this.show
   }
   ageIncrement(){
     this.person.age += 1;
   }


   //----------------------------event binding scroll ---------------------------------
   //($event) -> captura el evento en el dom
   onScroll(event: Event){
     const element = event.target as HTMLElement;
     console.log(element.scrollTop)
   }

 //prueba con object ----------------------------------------------------------
    person = {
      name:'Ruben',
      age:28
    }
   //--------------------------- event keyUp ---------------------------------
   onKeyUp(event: Event){
     //con los input hay una pequeña variacion
     const element = event.target as HTMLInputElement;
     this.person.name = element.value
   }

  constructor() { }

  ngOnInit(): void {
  }

}
