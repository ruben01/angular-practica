export interface Product {
    id:number
    title: string,
    price:number,
    image:string,
    description: string,
    category?:string // ? -> siginifica que al implementar el modelo la propiedad es opcional
}