import { Directive, ElementRef } from '@angular/core';

//ElementRef ->  es un servicio y sirve para modificar el dom

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {
  //2. injectmamos el servicio
  constructor(private element:ElementRef) { 
    //uso, nativeElement -> devuelve el elemento nativo del html
    this.element.nativeElement.style.backgroundColor = 'Red'
  }

}
