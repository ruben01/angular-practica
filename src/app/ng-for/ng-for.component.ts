import { Component, Input, OnInit } from '@angular/core';
import { Product } from '../product.model';

@Component({
  selector: 'app-ng-for',
  templateUrl: './ng-for.component.html',
  styleUrls: ['./ng-for.component.sass']
})


export class NgForComponent implements OnInit {

    //-------------------------- *ngFor array ---------------------------------
    names: string[] = ['Yahweh','Adonai','Elohim']
  
    //a------------------------- gregar elementos del input al array ----------
    addName(event:Event){
      const element = event.target as HTMLInputElement;
      this.names.push(element.value);
      element.value = '';
    }
    //-------------------------- eliminar datos del array por indice ----------
    deleteName(index:number){
      //splice, recibe el indice del array, y la cantidad de elementos a quitar
      this.names.splice(index,1)
    }
    @Input() input1:string =''
    editName(id:number, newName:string){
      const index = this.names.findIndex(elemento => elemento === newName);

      console.log(this.names[index]);
       this.input1 = this.names[index]
    }

    // ------------------------- array de objetos -----------------------------
    products: Product [] = [
      {
        name: 'EL mejor juguete',
        price: 565,
        image: 'https://source.unsplash.com/random',
        category: 'all',
      },
      {
        name: 'Colleción de albumnes',
        price: 34,
        image: 'https://source.unsplash.com/random'
      },
      {
        name: 'Mis libros',
        price: 23,
        image: 'https://source.unsplash.com/random'
      },
      {
        name: 'Casa para perro',
        price: 34,
        image: 'https://source.unsplash.com/random'
      },
      {
        name: 'Gafas',
        price: 3434,
        image: 'https://source.unsplash.com/random'
      },
      {
        name: 'Gafas',
        price: 3434,
        image: 'https://source.unsplash.com/random'
      },
      {
        name: 'Gafas',
        price: 3434,
        image: 'https://source.unsplash.com/random'
      },
      {
        name: 'Gafas',
        price: 3434,
        image: 'https://source.unsplash.com/random'
      }
    ]

    //--------------------------modelo(objeto) para formulario --------------------------
    registro = {
      nombre:'',
      correo:'',
      contrasena:''
    }

    //evento para registrar usuario
    registrarUsuario(){
      console.log(this.registro)
    }


  constructor() { }

  ngOnInit(): void {
  }

}
